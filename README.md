## Name
**Ship it!**
Node.js App

## Description
The App is a Node.js App that handles requests for shipping goods from one address to another.
It allows to post the load, assign to driver and follow up on the load movement.
Uses express and mongoose (do not forget to update the link to the database).

## Installation
Node.js version 14 or later is required for the App to run.
Steps:
- clone the repo
- update .env file
- run npm i
- npm start / npm run debug to run the application
- requests are listed in the openapi.yaml file. Paste it here - https://editor.swagger.io/ to navigate UI

## Authors and acknowledgment
If you have any questions or issues with the task manager app, please contact the developer (Yana).
This project is part of the Node.js assesment project.

## License
MIT License.

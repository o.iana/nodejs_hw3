/* eslint-disable max-len */
const requestInfo = document.querySelector('.request-info');
const driverResult = document.querySelector('.req-result');

const baseUrl = 'http://localhost:8080/api/trucks';
const loadsUrl = 'http://localhost:8080/api/loads';

const closeInfoDiv = () => {
    if(requestInfo){
        requestInfo.style.display === 'none';
        requestInfo.innerHTML = '';
    }
}

const openInfoDiv = () => {
    const requestId = document.querySelector('.request-id');
    if(requestId){
        closeInfoDiv();
    }else{
            const triggeredReq = event.target.className.split(' ')[0];
            requestInfo.style.display === 'block';
            requestInfo.innerHTML = 
            '<label for="request-id"><b>Add request details</b></label>'+
            '<input class="request-id" id="request-id" name="request-id" type="text" placeholder="Information" required>'+
            '<button class="submit-request" type="submit" for="request-btn"><b>Submit request</b></button>';
            const submitRequestBtn = document.querySelector('.submit-request');
                switch (triggeredReq) {
                case 'one-truck':
                    submitRequestBtn.addEventListener('click', getOneTruck, false);
                    break;
                case 'add-truck':
                    submitRequestBtn.addEventListener('click', addOneTruck, false);
                    break;
                case 'update-truck':
                    submitRequestBtn.addEventListener('click', updateOneTruck, false);
                    break;
                case 'delete-truck':
                    submitRequestBtn.addEventListener('click', deleteOneTruck, false);
                    break;
                case 'assign-truck':
                    submitRequestBtn.addEventListener('click', assignOneTruck, false);
                    break;
                case 'load-by-id':
                    submitRequestBtn.addEventListener('click', getSpecificLoad, false);
                    break;
                default:
                  console.log('THIS IS CASE default');
              }
    }
}

const getAllTrucks = async() => {
    removeInnerHTML();
    closeInfoDiv();
    const myToken = localStorage.getItem('User');
    const res = await fetch(`${baseUrl}`, {
        headers: {
        'Content-Type': 'application/json',
        'Authorization': myToken,
        }
})
if(res.status === 200){
    res.json().then((result) => {
        if(result.trucks.length){
            driverResult.innerHTML = `<p>${JSON.stringify(result.trucks)}</p>`
        }else{
            driverResult.innerHTML = `<p>NO TRUCKS FOR YOUR USER</p>`
        }
        
});
}else{
    res.json().then(() => {
        driverResult.innerHTML = `<p>Something went wrong, try again</p>`
    });
}
setTimeout(removeInnerHTML, 7000);
}

const getOneTruck = async() => {
    removeInnerHTML();
    closeInfoDiv();
    const requestId = document.querySelector('.request-id');
    const myToken = localStorage.getItem('User');
    const res = await fetch(`${baseUrl}/${requestId.value}`, {
        headers: {
        'Content-Type': 'application/json',
        'Authorization': myToken,
        }
})
if(res.status === 200){
    res.json().then((result) => {
        if(result){
            driverResult.innerHTML = `<p>
            <b>id</b>: ${JSON.stringify(result.truck._id)}
            <b>created by</b>: ${JSON.stringify(result.truck.created_by)}
            <b>assigned to</b>: ${JSON.stringify(result.truck.assigned_to)}
            <b>type</b>: ${JSON.stringify(result.truck.type)}
            <b>status</b>: ${JSON.stringify(result.truck.status)}
            <b>created date</b>: ${JSON.stringify(result.truck.created_date)}
            </p>`
        }else{
            driverResult.innerHTML = `<p>NO TRUCK FOUND BY THIS ID</p>`
        }
        
});
}else{
    res.json().then(() => {
        driverResult.innerHTML = `<p>Something went wrong, try again</p>`
    });
}
setTimeout(removeInnerHTML, 7000);
}

const updateOneTruck = async() => {
    removeInnerHTML();
    closeInfoDiv();
    const requestInfo = document.querySelector('.request-id');
    const requestArray = requestInfo.value.split(',');
    const requestID = requestArray[0];
    const requestChange = requestArray[1];
    const myToken = localStorage.getItem('User');
    const data = {
        requestChange
    }
    const res = await fetch(`${baseUrl}/${requestID}`, {
        method: 'PUT',
        headers: {
        'Content-Type': 'application/json',
        'Authorization': myToken,
        },
        body: JSON.stringify(data)
})
if(res.status === 200){
    res.json().then((result) => {
        if(result){
            driverResult.innerHTML = `<p>
            <b>result</b>: ${JSON.stringify(result.message)}
            </p>`
        }else{
            driverResult.innerHTML = `<p>NO TRUCK FOUND BY THIS ID</p>`
        }
        
});
}else{
    res.json().then(() => {
        driverResult.innerHTML = `<p>Something went wrong, try again</p>`
    });
}
setTimeout(removeInnerHTML, 7000);
}

const addOneTruck = async() => {
    removeInnerHTML();
    closeInfoDiv();
    const requestId = document.querySelector('.request-id');
    const myToken = localStorage.getItem('User');
    const data = {
        'type': requestId.value.toUpperCase()
    }
    const res = await fetch(`${baseUrl}`, {
        method: 'POST',
        headers: {
        'Content-Type': 'application/json',
        'Authorization': myToken,
        },
        body: JSON.stringify(data)
})
if(res.status === 200){
    res.json().then((result) => {
        if(result){
            driverResult.innerHTML = `<p><b>RESULT: </b>${JSON.stringify(result.message)}</p>`
        }else{
            driverResult.innerHTML = `<p>COULD NOT ADD TRUCK</p>`
        }
        
});
}else{
    res.json().then(() => {
        driverResult.innerHTML = `<p>Something went wrong, try again</p>`
    });
}
setTimeout(removeInnerHTML, 7000);
}

const deleteOneTruck = async() => {
    removeInnerHTML();
    closeInfoDiv();
    const requestId = document.querySelector('.request-id');
    const myToken = localStorage.getItem('User');
    const res = await fetch(`${baseUrl}/${requestId.value}`, {
        method: 'DELETE',
        headers: {
        'Content-Type': 'application/json',
        'Authorization': myToken,
        }
})
if(res.status === 200){
    res.json().then((result) => {
        if(result){
            driverResult.innerHTML = `<p><b>RESULT: </b>${JSON.stringify(result.message)}</p>`
        }else{
            driverResult.innerHTML = `<p>NO TRUCK FOUND BY THIS ID</p>`
        }
        
});
}else{
    res.json().then(() => {
        driverResult.innerHTML = `<p>Something went wrong, try again</p>`
    });
}
setTimeout(removeInnerHTML, 7000);
}

const assignOneTruck = async() => {
    removeInnerHTML();
    closeInfoDiv();
    const requestId = document.querySelector('.request-id');
    const myToken = localStorage.getItem('User');
    const res = await fetch(`${baseUrl}/${requestId.value}/assign`, {
        method: 'POST',
        headers: {
        'Content-Type': 'application/json',
        'Authorization': myToken,
        }
})
if(res.status === 200){
    res.json().then((result) => {
        if(result){
            driverResult.innerHTML = `<p><b>RESULT: </b>${JSON.stringify(result.message)}</p>`
        }else{
            driverResult.innerHTML = `<p>NO TRUCK FOUND BY THIS ID</p>`
        }
        
});
}else{
    res.json().then(() => {
        driverResult.innerHTML = `<p>Something went wrong, try again</p>`
    });
}
setTimeout(removeInnerHTML, 7000);
}

const iterateLoadState = async() => {
    removeInnerHTML();
    closeInfoDiv();
    const myToken = localStorage.getItem('User');
    const res = await fetch(`${loadsUrl}/active/state`, {
        method: 'PATCH',
        headers: {
        'Content-Type': 'application/json',
        'Authorization': myToken,
        }
})
if(res.status === 200){
    res.json().then((result) => {
            driverResult.innerHTML = 
            `<p><b>Result: </b>${JSON.stringify(result.message)}</p>`
});
}else{
    res.json().then(() => {
        driverResult.innerHTML = `<p>Something went wrong, try again</p>`
    });
}
setTimeout(removeInnerHTML, 10000);
}
const getAllLoads = async() => {
    removeInnerHTML();
    closeInfoDiv();
    const myToken = localStorage.getItem('User');
    const res = await fetch(`${loadsUrl}`, {
        headers: {
        'Content-Type': 'application/json',
        'Authorization': myToken,
        }
})
if(res.status === 200){
    res.json().then((result) => {
        if(result.loads.length){
            driverResult.innerHTML = `<p>${JSON.stringify(result.loads)}</p>`
        }else{
            driverResult.innerHTML = `<p>NO LOADS FOR YOUR USER</p>`
        }
        
});
}else{
    res.json().then(() => {
        driverResult.innerHTML = `<p>Something went wrong, try again</p>`
    });
}
setTimeout(removeInnerHTML, 7000);
}

const getActiveLoad = async() => {
    removeInnerHTML();
    closeInfoDiv();
    const myToken = localStorage.getItem('User');
    const res = await fetch(`${loadsUrl}/active`, {
        headers: {
        'Content-Type': 'application/json',
        'Authorization': myToken,
        }
})
if(res.status === 200){
    res.json().then((result) => {
        if(result.load){
            driverResult.innerHTML = `<p>
            <b>id: </b>${JSON.stringify(result.load._id)}
            <b>name: </b>${JSON.stringify(result.load.name)}
            <b>status: </b>${JSON.stringify(result.load.status)}
            <b>state: </b>${JSON.stringify(result.load.state)}
            <b>dimensions: </b>${JSON.stringify(result.load.dimensions)}
            <b>payload: </b>${JSON.stringify(result.load.payload)}
            <b>pickup address: </b>${JSON.stringify(result.load.pickup_address)}
            <b>created date: </b>${JSON.stringify(result.load.created_date)}
            <b>logs: </b>${JSON.stringify(result.load.logs)}
            </p>`
        }else{
            driverResult.innerHTML = `<p>NO LOADS FOR YOUR USER</p>`
        }      
});
}else{
    res.json().then(() => {
        driverResult.innerHTML = `<p>Something went wrong, try again</p>`
    });
}
setTimeout(removeInnerHTML, 10000);
}

const getSpecificLoad = async() => {
    removeInnerHTML();
    closeInfoDiv();
    const requestId = document.querySelector('.request-id');
    const myToken = localStorage.getItem('User');
    const res = await fetch(`${loadsUrl}/${requestId.value}`, {
        headers: {
        'Content-Type': 'application/json',
        'Authorization': myToken,
        }
})
if(res.status === 200){
    res.json().then((result) => {
        if(result.load){
            driverResult.innerHTML = `<p>
            <b>id: </b>${JSON.stringify(result.load._id)}
            <b>name: </b>${JSON.stringify(result.load.name)}
            <b>status: </b>${JSON.stringify(result.load.status)}
            <b>state: </b>${JSON.stringify(result.load.state)}
            <b>dimensions: </b>${JSON.stringify(result.load.dimensions)}
            <b>payload: </b>${JSON.stringify(result.load.payload)}
            <b>pickup address: </b>${JSON.stringify(result.load.pickup_address)}
            <b>created date: </b>${JSON.stringify(result.load.created_date)}
            <b>logs: </b>${JSON.stringify(result.load.logs)}
            </p>`
        }else{
            driverResult.innerHTML = `<p>NO LOADS FOUND</p>`
        }      
});
}else{
    res.json().then(() => {
        driverResult.innerHTML = `<p>Something went wrong, try again</p>`
    });
}
setTimeout(removeInnerHTML, 10000);
}


function removeInnerHTML() {
    if(driverResult){
        driverResult.innerHTML = '';
    }
}
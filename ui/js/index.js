/* eslint-disable no-unused-vars */
/* eslint-disable no-magic-numbers */
const userEmail = document.querySelector('.email');
const userPassword = document.querySelector('.password');
const userRole = document.querySelector('.role');
const baseUrl = 'http://localhost:8080/api/auth';
const responseDiv = document.querySelector('.response');
const pageLinks = document.querySelector('.links');

async function register () {
    const checker = /\S+@\S+\.\S+/;
    if(!checker.test(userEmail.value)){
        alert('Please check email is correct');
        return
    }
    if(!userRole.value) {
        alert('Please add your role');
        return
    } 
    let data = {
        'email': userEmail.value,
        'password': userPassword.value,
        'role': userRole.value
    }
    const res = await fetch(`${baseUrl}/register`, {
        method: 'POST',
            headers: {
            'Content-Type': 'application/json'
            },
        body: JSON.stringify(data)
})
if(res.status === 200){
    console.log('ALL GOOD YOU ARE REGISTERED');
    responseDiv.innerHTML = JSON.stringify('Registered successfully!');
}else{
    res.json().then((data) => {
        responseDiv.innerHTML = JSON.stringify(data.message) 
       });
}
setTimeout(removeInnerHTML, 5000);
}

const login = async() => {
    const emailCheck = /\S+@\S+\.\S+/;
    if(!emailCheck.test(userEmail.value)){
        alert('Please check email is correct');
        return
    }
    let data = {
        'email': userEmail.value,
        'password': userPassword.value,
    }
    const res = await fetch(`${baseUrl}/login`, {
        method: 'POST',
            headers: {
            'Content-Type': 'application/json'
            },
        body: JSON.stringify(data)
})
if(res.status === 200){
    res.json().then((data) => {
        localStorage.setItem('User', `Bearer ${data.jwt_token}`);
       });
    responseDiv.innerHTML = JSON.stringify('Welcome, friend!');
    const myInfo = document.querySelector('.my-info');
    if(!myInfo){
        pageLinks.innerHTML += "<a class = 'my-info' href = './user.html'>My Info</a>";
    }
    console.log('ALL GOOD YOU ARE LOGGED IN');
}else{
    res.json().then((data) => {
        responseDiv.innerHTML = JSON.stringify(data.message)
       });
}
setTimeout(removeInnerHTML, 5000);
}

const emailPassword = async() => {
    const emailCheck = /\S+@\S+\.\S+/;
    if(!emailCheck.test(userEmail.value)){
        alert('Please check email is correct');
        return
    }
    let data = {
        'email': userEmail.value,
    }
    const res = await fetch(`${baseUrl}/forgot_password`, {
        method: 'POST',
            headers: {
            'Content-Type': 'application/json'
            },
        body: JSON.stringify(data)
})
if(res.status === 200){
    res.json().then((data) => {
 responseDiv.innerHTML = JSON.stringify(data.message);
});
}else{
    res.json().then((data) => {
        responseDiv.innerHTML = JSON.stringify(data.message) 
       });
}
setTimeout(removeInnerHTML, 5000);
}


function removeInnerHTML() {
    responseDiv.innerHTML = '';
}
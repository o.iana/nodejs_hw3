/* eslint-disable no-magic-numbers */
/* eslint-disable no-unused-vars */
/* eslint-disable max-len */
const requestInfo = document.querySelector('.request-info');
const shipperResult = document.querySelector('.req-result');

const loadsUrl = 'http://localhost:8080/api/loads';

const closeInfoDiv = () => {
    if(requestInfo){
        requestInfo.style.display === 'none';
        requestInfo.innerHTML = '';
    }
}

const openInfoDiv = () => {
    const requestId = document.querySelector('.request-id');
    if(requestId){
        closeInfoDiv();
    }else{
            const triggeredReq = event.target.className.split(' ')[0];
            requestInfo.style.display === 'block';
            requestInfo.innerHTML = 
            '<label for="request-id"><b>Add request details</b></label>'+
            '<input class="request-id" id="request-id" name="request-id" type="text" placeholder="Information" required>'+
            '<button class="submit-request" type="submit" for="request-btn"><b>Submit request</b></button>';
            const submitRequestBtn = document.querySelector('.submit-request');
                switch (triggeredReq) {
                case 'get-load-info':
                    submitRequestBtn.addEventListener('click', getLoadInfo, false);
                    break;
                case 'add-load':
                    submitRequestBtn.addEventListener('click', addLoad, false);
                    break;
                case 'update-load':
                    submitRequestBtn.addEventListener('click', updateLoad, false);
                    break;
                case 'delete-load':
                    submitRequestBtn.addEventListener('click', deleteOneLoad, false);
                    break;
                case 'post-load':
                    submitRequestBtn.addEventListener('click', postLoad, false);
                    break;
                case 'load-by-id':
                    submitRequestBtn.addEventListener('click', getSpecificLoad, false);
                    break;
                default:
                  console.log('THIS IS CASE default');
              }
    }
}

const getAllLoads = async() => {
    removeInnerHTML();
    const myToken = localStorage.getItem('User');
    const res = await fetch(`${loadsUrl}`, {
        headers: {
        'Content-Type': 'application/json',
        'Authorization': myToken,
        }
})
if(res.status === 200){
    res.json().then((result) => {
        if(result.loads.length){
            shipperResult.innerHTML = `<p>${JSON.stringify(result.loads)}</p>`
        }else{
            shipperResult.innerHTML = `<p>NO LOADS FOR THIS USER</p>`
        }
});
}else{
    res.json().then(() => {
        shipperResult.innerHTML = `<p>Something went wrong, try again</p>`
    });
}
setTimeout(removeInnerHTML, 10000);
}

const getLoadInfo = async() => {
    removeInnerHTML();
    const requestId = document.querySelector('.request-id');
    const myToken = localStorage.getItem('User');
    const res = await fetch(`${loadsUrl}/${requestId.value}/shipping_info`, {
        headers: {
        'Content-Type': 'application/json',
        'Authorization': myToken,
        }
})
if(res.status === 200){
    res.json().then((result) => {
        if(result){
            shipperResult.innerHTML = `<p>
            <b>ID</b>: ${JSON.stringify(result.load._id)}
            <b>CREATED BY</b>: ${JSON.stringify(result.load.created_by)}
            <b>ASSIGNED TOo</b>: ${JSON.stringify(result.load.assigned_to)}
            <b>TYPE</b>: ${JSON.stringify(result.load.type)}
            <b>STATUS</b>: ${JSON.stringify(result.load.status)}
            <b>CREATED DATE</b>: ${JSON.stringify(result.load.created_date)}
            <b>LOGS</b>: ${JSON.stringify(result.load.logs)}
            </p>`
        }else{
            shipperResult.innerHTML = `<p>NO TRUCK FOUND BY THIS ID</p>`
        }
});
}else{
    res.json().then((result) => {
        shipperResult.innerHTML = `<p>Something went wrong, try again</p>`
    });
}
setTimeout(removeInnerHTML, 7000);
}


const updateLoad = async() => {
    removeInnerHTML();
    const requestInfo = document.querySelector('.request-id');
    if(!requestInfo.value){
        alert('PLEASE ADD ID & UPDATE INFORMATION')
    }
    const requestArray = requestInfo.value.split(',');
    const requestID = requestArray[0];
    const requestChange = requestArray[1];
    const myToken = localStorage.getItem('User');
    const res = await fetch(`${loadsUrl}/${requestID}`, {
        method: 'PUT',
        headers: {
        'Content-Type': 'application/json',
        'Authorization': myToken,
        },
        body: JSON.stringify(JSON.parse(requestChange))
})
if(res.status === 200){
    res.json().then((result) => {
        if(result){
            shipperResult.innerHTML = `<p>
            <b>result</b>: ${JSON.stringify(result.message)}
            </p>`
        }else{
            shipperResult.innerHTML = `<p>NO LOADS FOUND BY THIS ID</p>`
        }
});
}else{
    res.json().then(() => {
        shipperResult.innerHTML = `<p>Something went wrong, try again</p>`
    });
}
setTimeout(removeInnerHTML, 7000);
}


const addLoad = async() => {
    removeInnerHTML();
    const requestId = document.querySelector('.request-id');
    const myToken = localStorage.getItem('User');
    const data = requestId.value;
    const res = await fetch(`${loadsUrl}`, {
        method: 'POST',
        headers: {
        'Content-Type': 'application/json',
        'Authorization': myToken,
        },
        body: data
})
if(res.status === 200){
    res.json().then((result) => {
        if(result){
            shipperResult.innerHTML = `<p><b>RESULT: </b>${JSON.stringify(result.message)}</p>`
        }else{
            shipperResult.innerHTML = `<p>COULD NOT ADD LOAD</p>`
        }
});
}else{
    res.json().then(() => {
        shipperResult.innerHTML = `<p>Something went wrong, try again</p>`
    });
}
setTimeout(removeInnerHTML, 7000);
setTimeout(closeInfoDiv, 10000);
}


const deleteOneLoad = async() => {
    removeInnerHTML();
    const requestId = document.querySelector('.request-id');
    const myToken = localStorage.getItem('User');
    const res = await fetch(`${loadsUrl}/${requestId.value}`, {
        method: 'DELETE',
        headers: {
        'Content-Type': 'application/json',
        'Authorization': myToken,
        }
})
if(res.status === 200){
    res.json().then((result) => {
        if(result){
            shipperResult.innerHTML = `<p><b>RESULT: </b>${JSON.stringify(result.message)}</p>`
        }else{
            shipperResult.innerHTML = `<p>NO LOAD FOUND BY THIS ID</p>`
        }
});
}else{
    res.json().then(() => {
        shipperResult.innerHTML = `<p>Something went wrong, try again</p>`
    });
}
setTimeout(removeInnerHTML, 7000);
}


const postLoad = async() => {
    removeInnerHTML();
    const requestId = document.querySelector('.request-id');
    const myToken = localStorage.getItem('User');
    const res = await fetch(`${loadsUrl}/${requestId.value}/post`, {
        method: 'POST',
        headers: {
        'Content-Type': 'application/json',
        'Authorization': myToken,
        }
})
if(res.status === 200){
    res.json().then((result) => {
        if(result){
            shipperResult.innerHTML = `<p><b>RESULT: </b>${JSON.stringify(result.message)}</p>`
        }else{
            shipperResult.innerHTML = `<p>NO LOAD FOUND BY THIS ID</p>`
        }
});
}else{
    res.json().then((result) => {
        shipperResult.innerHTML = `<p>${result.message}</p>`
    });
}
setTimeout(removeInnerHTML, 7000);
setTimeout(closeInfoDiv, 10000);
}


const getSpecificLoad = async() => {
    removeInnerHTML();
    const requestId = document.querySelector('.request-id');
    const myToken = localStorage.getItem('User');
    const res = await fetch(`${loadsUrl}/${requestId.value}`, {
        headers: {
        'Content-Type': 'application/json',
        'Authorization': myToken,
        }
})
if(res.status === 200){
    res.json().then((result) => {
        if(result.load){
            shipperResult.innerHTML = `<p>
            <b>id: </b>${JSON.stringify(result.load._id)}
            <b>name: </b>${JSON.stringify(result.load.name)}
            <b>status: </b>${JSON.stringify(result.load.status)}
            <b>state: </b>${JSON.stringify(result.load.state)}
            <b>dimensions: </b>${JSON.stringify(result.load.dimensions)}
            <b>payload: </b>${JSON.stringify(result.load.payload)}
            <b>pickup address: </b>${JSON.stringify(result.load.pickup_address)}
            <b>created date: </b>${JSON.stringify(result.load.created_date)}
            <b>logs: </b>${JSON.stringify(result.load.logs)}
            </p>`
        }else{
            shipperResult.innerHTML = `<p>NO LOADS FOUND</p>`
        }
});
}else{
    res.json().then(() => {
        shipperResult.innerHTML = `<p>Something went wrong, try again</p>`
    });
}
setTimeout(removeInnerHTML, 10000);
}


function removeInnerHTML() {
    if(shipperResult){
        shipperResult.innerHTML = '';
    }
}
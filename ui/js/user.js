/* eslint-disable no-magic-numbers */
const userId = document.querySelector('.user_id');
const userEmail = document.querySelector('.user_email');
const userRole = document.querySelector('.user_role');
const createdDate = document.querySelector('.created_date');
const pageLinks = document.querySelector('.links');
const changePassDiv = document.querySelector('.changePassDiv');
const changePassResult = document.querySelector('.changePassResult');
const oldPass = document.querySelector('.old-pass');
const newPass = document.querySelector('.new-pass');

const baseUrl = 'http://localhost:8080/api/users';

document.addEventListener('DOMContentLoaded', function(){
    const myToken = localStorage.getItem('User');
    getUserInfo(myToken);
});

const getUserInfo = async(myToken) => {
    const res = await fetch(`${baseUrl}/me`, {
            headers: {
            'Content-Type': 'application/json',
            'Authorization': myToken,
            }
    })
    if(res.status === 200){
        res.json().then((data) => {
     userId.innerText = data.user._id;
     userEmail.innerText = data.user.email;
     userRole.innerText = data.user.role;
     createdDate.innerText = data.user.created_date;
     data.user.role === 'SHIPPER'? pageLinks.innerHTML += "<a href = './shipperLoads.html'>My Loads</a>" : 
     pageLinks.innerHTML += "<a href = './driverTrucks.html'>My Trucks/Loads</a>"
    });
    }else{
        res.json().then((data) => {
            console.log('***** this is data ******', data);
           });
    }
}


const userLogout = () => {
    localStorage.removeItem('User');
    location.href = 'index.html';
    // return false;
}

const changePassModalToggle = () => {
    changePassDiv.style.display === 'none'? changePassDiv.style.display = 'block':changePassDiv.style.display = 'none';
}

const changeUserPass = async() => {
    const myToken = localStorage.getItem('User');
    const data = {
        'oldPassword': oldPass.value,
        'newPassword': newPass.value
    }
    const res = await fetch(`${baseUrl}/me/password`, {
        method: 'PATCH',
            headers: {
            'Content-Type': 'application/json',
            'Authorization': myToken,
            },
        body: JSON.stringify(data)
})
if(res.status === 200){
    res.json().then((result) => {
    changePassResult.innerHTML = `<p>${result.message}</p>`
});
}else{
    res.json().then((result) => {
    changePassResult.innerHTML = `<p>${result.message}</p>`
    });
}
setTimeout(removeInnerHTML, 5000);
}

function removeInnerHTML() {
    changePassResult.innerHTML = '';
}

const getTruckDimensions = (type) => {
  switch (type) {
    case 'SPRINTER':
        return {
            length: 300,
            width: 250,
            height: 170,
            payload:1700
        };
    case 'SMALL STRAIGHT':
        return {
            length: 500,
            width: 250,
            height: 170,
            payload:2500
        };
    case 'LARGE STRAIGHT':
        return {
            length: 700,
            width: 350,
            height: 200,
            payload:4000
        };
    default:
      return 'Truck type not found';
}
};

module.exports = { getTruckDimensions };
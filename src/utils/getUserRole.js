const { User } = require('../models/Users');

const getUserRole = async (userId) => {
  const reqUser = await User.findOne({_id:userId});
  return reqUser.role.toLowerCase()
};

module.exports = { getUserRole };
const jwt = require('jsonwebtoken');

const getUserId = (token) => {
  if (!token) {
 return new Error({ message: 'User not authorized' }); 
}
  try {
    const decodedData = jwt.verify(token, process.env.JWT_SECRET);
    return decodedData.userId;
  } catch (error) {
    return new Error(error);
  }
};

module.exports = { getUserId };

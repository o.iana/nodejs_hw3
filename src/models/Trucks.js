const mongoose = require('mongoose');
const Joi = require('joi');

const truckJoiSchema = Joi.object({
  type: Joi.string()
    .alphanum()
    .required()
    .valid('SPRINTER', 'SMALL STRAIGHT', 'LARGE STRAIGHT'),
});

const Truck = mongoose.model('Truck', {
  created_by: {
    type: String,
    required: true,
  },
  assigned_to: {
    type: String,
    required: true,
  },
  type: {
    type: String,
    required: true
  },
  status: {
    type: String,
    required: true,
  },
  created_date: {
    type: String,
    required: true,
  }
});

module.exports = {
  Truck,
  truckJoiSchema
};

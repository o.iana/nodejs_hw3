const mongoose = require('mongoose');
const Joi = require('joi');

const loadJoiSchema = Joi.object({
  name: Joi.string()
    .required()
    .min(2)
    .required(),

  payload: Joi.number()
    .integer()
    .min(1)
    .required(),

  pickup_address: Joi.string()
    .alphanum()
    .min(2)
    .required(),

  delivery_address: Joi.string()
    .alphanum()
    .min(2)
    .required(),

  dimensions: Joi.object({
    width: Joi.number().min(1).required(),
    length: Joi.number().min(1).required(),
    height: Joi.number().min(1).required(),
  }).required()
});

const Load = mongoose.model('Load', {
  created_by: {
    type: String,
    required: true,
      },
  assigned_to: {
    type: String,
    required: true,
      },
  status: {
    type: String,
    required: true,
    },
  state: {
    type: String,
    required: true,
    },
  name: {
    type: String,
    required: true,
  },
  payload: {
    type: Number,
    required: true,
  },
  pickup_address: {
    type: String,
    required: true,
  },
  delivery_address: {
    type: String,
    required: true,
  },
  dimensions: {
    width: {
        type: Number,
        required: true,
    },
    length: {
        type: Number,
        required: true,
    },
    height: {
        type: Number,
        required: true,
    }
  },
  logs: {
    type: Array,
    optional: true,
    default : [],
  },
  created_date: {
    type: String,
    required: true,
  },
  delivery_truck: {
    type: String,
    optional: true,
  },
});

module.exports = {
  Load,
  loadJoiSchema
};
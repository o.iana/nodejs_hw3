/* eslint-disable no-magic-numbers */
const { User, userJoiSchema } = require('../models/Users.js');
const bcryptjs = require('bcryptjs');
const jwt = require('jsonwebtoken');
const { saveUser } = require('../services/usersService');

const registerUser = async (req, res, next) => {
  try {
    const { email, password, role } = req.body;
    await userJoiSchema.validateAsync({email, password, role});
    const created_date = new Date().toISOString();
    await saveUser({ email, password, role, created_date });
    return res.json({message: 'Profile created successfully'});
  } catch (error) {
    next(error);
  }
}

const loginUser = async (req, res) => {
  const user = await User.findOne({ email: req.body.email });
  if (user && await bcryptjs.compare(String(req.body.password), String(user.password))) {
    const payload = { email: user.email, password: user.password, userId: user._id };
    const jwtToken = jwt.sign(payload, process.env.JWT_SECRET);
    return res.json({jwt_token: jwtToken});
  }
  return res.status(403).json({'message': 'Not authorized'});
}

const restorePass = async(req, res, next) => {
  try {
    const user = await User.findOne({ email: req.body.email });
    if(!user) {
    return res.status(400).json({message: 'User is not Authorized'});
}
    return res.json({message: 'New password sent to your email address'});
  } catch (error) {
    next(error);
  }
}

module.exports = {
  registerUser,
  loginUser,
  restorePass,
};

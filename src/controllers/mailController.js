/* eslint-disable no-magic-numbers */
const jwt = require('jsonwebtoken')
const { User } = require('../models/Users.js');
const { transporter, getPasswordResetURL, resetPasswordTemplate } = require('../services/mailService');

const usePasswordHashToMakeToken = ({
    password: passwordHash,
    _id: userId,
  }) => {
    const secret = passwordHash + '-' + Date.now();
    const token = jwt.sign({ userId }, secret, {
      expiresIn: 3600 // 1 hour
    })
    return token
  }

const sendPasswordResetEmail = async (req, res) => {
    const { email } = req.body
    let user 
    try {
      user = await User.findOne({ email });
    } catch (err) {
      res.status(404).json({message:'No user with that email'})
    }
    const token = usePasswordHashToMakeToken(user)
    const url = getPasswordResetURL(user, token)
    const emailTemplate = resetPasswordTemplate(user, url)

    const sendEmail = () => {
      transporter.sendMail(emailTemplate, (err, info) => {
        if (err) {
          res.status(500).json({message:'Error sending email', err})
        }
        console.log(`** Email sent **`, info.response)
      })
    }
    sendEmail()
  }


  module.exports = {usePasswordHashToMakeToken, sendPasswordResetEmail}
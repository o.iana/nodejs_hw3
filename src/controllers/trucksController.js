/* eslint-disable no-magic-numbers */
/* eslint-disable max-len */
/* eslint-disable consistent-return */
const { Truck, truckJoiSchema } = require('../models/Trucks');
const { getUserRole } = require('../utils/getUserRole');

const addTruck = async (req, res, next) => {
  if (!req.body || !req.body.type) {
 return res.status(400).json({ message: 'Type field is required' }) 
}
  const { type } = req.body;
  const uID = req.user.userId;
  if (typeof uID !== 'string') {
    return res.status(400).json({ message: 'User not authorized' }); 
   }
   const role = await getUserRole(req.user.userId);
  if(role !== 'driver') {
 return res.status(400).json({ message: 'Your role cannot perform this action' })
}
  const date = new Date();
  try {
    await truckJoiSchema.validateAsync({type});
    const truck = new Truck({
      created_by:uID,
      assigned_to: 'NA',
      type,
      status:'IS',
      created_date: date.toISOString(),
    });
    return truck.save().then(() => res.status(200).json({ message: 'Truck created successfully' }));
  } catch (error) {
    next(error);
  }
};

const getTruck = async (req, res, next) => {
const role = await getUserRole(req.user.userId);
if(role !== 'driver') {
return res.status(400).json({ message: 'Your role cannot perform this action' })
}
  try {
    Truck.findById(req.params.id)
      .then((truck) => res.status(200).json({ truck }))
      .catch((error) => res.status(500).json({ message: error.message }));
  } catch (error) {
    next(error);
  }
};

const updateTruck = async (req, res, next) => {
  if (!req.body) {
 return res.status(400).json({ message: 'More information is required' }); 
}
const role = await getUserRole(req.user.userId);
if(role !== 'driver') {
return res.status(400).json({ message: 'Your role cannot perform this action' })
}
  try {
    const truck = await Truck.findById(req.params.id).catch((error) => res.status(400).json({ message: error.message }));
    const { type } = req.body;

    if (type) {
          truck.type = type;   
      }

    return truck.save().then((saved) => res.status(200).json({ message: 'Truck details changed successfully', saved }))
    .catch((error) => res.status(500).json({ message: error.message }));
  } catch (error) {
    next(error);
  }
};

const deleteTruck = async (req, res, next) => {
  const role = await getUserRole(req.user.userId);
  if(role !== 'driver') {
  return res.status(400).json({ message: 'Your role cannot perform this action' })
  }
  try {
    Truck.findByIdAndDelete(req.params.id)
      .then(() => res.status(200).json({ message: 'Truck deleted successfully' }))
      .catch(() => {
 res.status(400).json({ message: 'Something went wrong, try again'}) 
});
  } catch (error) {
    next(error)
  }
};

const getAllTrucks = async(req, res, next) => {
  const role = await getUserRole(req.user.userId);
  if(role !== 'driver') {
  return res.status(400).json({ message: 'Your role cannot perform this action' })
  }
  try {
    Truck.find(
      // { assigned_to: req.user.userId}
      {$or: [
        {assigned_to: req.user.userId},
        {created_by: req.user.userId}
    ]}
      , '-__v')
    .then((result) => {
      res.json({ trucks: result });
    })
    .catch(() => res.status(400).json({ message: 'Something went wrong, try again'}));
  } catch (error) {
    // return res.status(500).json({ message: error });
    next(error);
  }
};

const assignTruckToUser = async(req, res) => {
  const role = await getUserRole(req.user.userId);
  if(role !== 'driver') {
  return res.status(400).json({ message: 'Your role cannot perform this action' })
  }
    try {
     const anyAssignedToUser = await Truck.findOne({ assigned_to: req.user.userId });
     if(anyAssignedToUser){
      res.status(400).json({ message: 'This driver already has assigned truck'});
     } else{
      await Truck.updateOne({ _id: req.params.id}, { $set: { assigned_to: req.user.userId} })
      .then((result) => {
 res.status(200).json({ message: 'Truck assigned successfully', result }) 
})
      .catch(() => res.status(500).json({ message: 'Could not assign truck, try again'}));
     }
    } catch (error) {
      return res.status(500).json({ message: error });
    }
  };

module.exports = {
  addTruck,
  getTruck,
  updateTruck,
  deleteTruck,
  getAllTrucks,
  assignTruckToUser,
};


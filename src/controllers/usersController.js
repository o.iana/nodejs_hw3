/* eslint-disable no-magic-numbers */
const { User } = require('../models/Users.js');
const bcryptjs = require('bcryptjs');
const jwt = require('jsonwebtoken');
const { getUserId } = require('../utils/getUserId');

const getUserInfo = async (req, res, next) => {
  const token = req.headers.authorization.split(' ')[1];
  const decodedData = jwt.verify(token, process.env.JWT_SECRET);
  if (!decodedData.email) {
  return res.status(400).json({ message: 'User not found' }) 
}
  try {
    const userData = await User.findOne({ email: decodedData.email });
    const userInfo = {
      // eslint-disable-next-line no-underscore-dangle
      _id: userData._id,
      role: userData.role,
      email: userData.email,
      created_date: userData.created_date,
    };
    return res.status(200).json({ user: userInfo });
  } catch (error) {
    next(error);
  }
};

const updateUserPassword = async (req, res, next) => {
  try {
    const token = req.headers.authorization.split(' ')[1];
    const id = getUserId(token);
    const newHashPass = await bcryptjs.hash(req.body.newPassword, 10);
    User.findByIdAndUpdate({ _id: id }, { $set: { password: newHashPass } })
      .then(() => {
        res.json({ message: 'Password changed successfully' });
      });
  } catch (error) {
    next(error);
  }
};

const deleteUser = (req, res, next) => {
  try {
    const token = req.headers.authorization.split(' ')[1];
    const id = getUserId(token);
    User.findByIdAndDelete({ _id: id })
      .then((data) => {
        if(!data) {
            res.status(404).json({ message: 'User not found' }); 
                  }
        res.status(200).json({ message: 'Profile deleted successfully' }) 
});
  } catch (error) {
    next(error);
  }
};

module.exports = {
  getUserInfo,
  updateUserPassword,
  deleteUser,
};

/* eslint-disable no-magic-numbers */
/* eslint-disable max-len */
const { Load } = require('../models/Load');
const { Truck } = require('../models/Trucks');

const { getUserRole } = require('../utils/getUserRole');
const { check4Trucks } = require('../services/loadsService')

async function addLoad(req, res, next) {
  const { name, payload, pickup_address, delivery_address, dimensions: {width, length, height} } = req.body;
  const role = await getUserRole(req.user.userId);
  if(role !== 'shipper') {
  return res.status(401).json('Your roles is not authorized to post load'); 
  }
  const date = new Date();
  const load = new Load({
    created_by: req.user.userId,
    assigned_to: 'NA',
    status: 'NEW',
    state: 'NEW',
    name,
    payload,
    pickup_address,
    delivery_address,
    dimensions: {width, length, height},
    logs: [],
    created_date:date.toISOString(),
  });
  try {
    load.save().then(() => {
      res.json({message: 'Load created successfully'});
    })
    .catch((error) => res.status(500).json(error.message));
  } catch (error) {
    next(error);
  }
}

async function getUserLoads(req, res, next) {
  const role = await getUserRole(req.user.userId);
  try {
    if(role === 'shipper'){
      return Load.find(
        {created_by:req.user.userId}, '-__v'
      ).then((result) => {
        res.json({loads:result});
      }).catch(() => res.status(500).json({message:'No NEW Loads found'}));
    } else if(role === 'driver') {
      return Load.find(
        {assigned_to:req.user.userId}
      ).then((result) => {
        res.json({loads:result});
      }).catch(() => res.status(500).json({message:'No completed or active Loads found'})); 
  }
  } catch (error) {
    next(error);
  }
}

const getUserLoadById = (req, res, next) => {
  try {
    Load.findById(req.params.id)
    .then((result) => {
    res.json({load:result});
  }).catch(() => res.status(500).json({message:'Something went wrong'}));
  } catch (error) {
    next(error)
  }
}

const getLoadShipInfo = async (req, res, next) => {
  const role = await getUserRole(req.user.userId);
  if(role !== 'shipper') {
    return res.status(401).json('Your roles is not authorized to perform this action'); 
    }
    try {
      const load = await Load.findOne({_id:req.params.id}, '-__v');
      const truck = await Truck.findOne({_id:load.delivery_truck}, '-__v');
      res.status(200).json({load, truck});
    } catch (error) {
      next(error)
    }
};

const postUserLoadById = async (req, res, next) => {
  const role = await getUserRole(req.user.userId);
  if(role !== 'shipper') {
    return res.status(401).json('Your roles is not authorized to perform this action'); 
    }
  const myLoad = await Load.findById(req.params.id);
  try {
    const myTruckType = check4Trucks(myLoad);
    if(!myTruckType) {
      myLoad.logs.push({
        message: 'No matching trucks available',
        time: new Date().toISOString(),
      })
      myLoad.save();
      return res.status(400).json({message:'No matching trucks available atm'}); 
      }
    const myTruck = await Truck.find({status:'IS', type: myTruckType, assigned_to: { $ne: 'NA' }});
    if(!myTruck.length) {
      return res.status(400).json({message:'No matching trucks available atm'}); 
      }
    await Truck.updateOne({_id: myTruck[0]._id }, {status:'OL'});
    await Load.updateOne({_id:myLoad._id}, {assigned_to: myTruck[0].assigned_to, status: 'ASSIGNED', state: 'En route to Pick Up', delivery_truck:myTruck[0]._id});
    return res.status(200).json({message: 'Load posted successfully','driver_found': true})
  } catch (error) {
    next(error);
  }
};

const deleteUserLoadById = async(req, res, next) => {
  const role = await getUserRole(req.user.userId);
  if(role !== 'shipper') {
    return res.status(401).json('Your roles is not authorized to delete load'); 
    }
  try {
    await Load.findByIdAndDelete(req.params.id)
  .then(() => {
    res.json({message: 'Load deleted successfully'});
  })
  .catch(() => res.json({message:'Load was not deleted'}));
  } catch (error) {
    next(error)
  }
}

const getActiveLoad = async (req, res, next) => {
  const role = await getUserRole(req.user.userId);
  try {
    if(role !== 'driver') {
      return res.status(401).json('Your roles is not authorized to perform this action'); 
      }
    return Load.find({assigned_to:req.user.userId}, '-__v').then((result) => {
      res.json({load:result[0]});
    }).catch(() => ({message:'No active loads found'}));
  } catch (error) {
    next(error)
  }
}

const updateUserLoadById = (req, res, next) => {
  try {
    const {name, payload, pickup_address, dilevery_address, dimensions} = req.body;
    return Load.findByIdAndUpdate({_id: req.params.id}, 
      {$set: { name, payload, pickup_address, dilevery_address, dimensions } })
      .then(() => {
        res.json({message: 'Load details changed successfully'});
      })
      .catch(() => res.json({message:'Could not update the load'}));
    
  } catch (error) {
    next(error);
  }
}

const iterateLoadState = async (req, res, next) => {
  try {
  let states = ['En route to Pick Up', 'Arrived to Pick Up', 'En route to delivery', 'Arrived to delivery']
  const myLoad = await Load.findOne(
    {assigned_to: req.user.userId}
  );
 const stateIndex = states.findIndex((element) => element.toLowerCase() === myLoad.state.toLowerCase());
 if(stateIndex>=0 && stateIndex<states.length-1){
    myLoad.logs.push({
      message: `Load state ${states[stateIndex+1]}`,
      time: new Date().toISOString(),
    })
    myLoad.save();
    if(stateIndex+1 === states.length-1){
      await Load.updateOne({assigned_to: req.user.userId}, {
        status: 'SHIPPED'});
        // myLoad.logs.push({
        //   message: 'Load is Shipped',
        //   time: new Date().toISOString(),
        // })
    }
    await Load.updateOne({assigned_to: req.user.userId}, {
      state: states[stateIndex+1],
  })
      res.json({message: `Load state changed to ${states[stateIndex+1]}`});
 } else if(stateIndex === states.length-1) {
    // await Load.updateOne({assigned_to: req.user.userId}, {
    // status: 'SHIPPED'});
    myLoad.logs.push({
      message: 'Load is Shipped',
      time: new Date().toISOString(),
    })
    myLoad.save();
    await Truck.updateOne({_id: myLoad.delivery_truck}, {$set: { status: 'IS' } })
    res.json({message:'Your load is shipped'}); 
} else {
  res.json({message:'Could not update the load state'}); 
}   
  } catch (error) {
    next(error);
  }

}

module.exports = {
  getUserLoads, 
  addLoad,
  getActiveLoad, 
  iterateLoadState,
  getUserLoadById, 
  updateUserLoadById, 
  deleteUserLoadById,
  postUserLoadById, 
  getLoadShipInfo
};

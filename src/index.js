const express = require('express');
const morgan = require('morgan');
const cors=require('cors');
require('dotenv').config();

const app = express();
const mongoose = require('mongoose');

// eslint-disable-next-line max-len
mongoose.connect('mongodb+srv://HW3Cluster:HW3ClusterPass@cluster0.hyqsdqc.mongodb.net/HW3?retryWrites=true&w=majority')
.then(() => console.log('MongoDB connected'))
.catch((error) => console.log('Issue connecting to MongoDB', error));

const { trucksRouter } = require('./routers/trucksRouter');
const { usersRouter } = require('./routers/usersRouter');
const { authRouter } = require('./routers/authRouter');
const { loadsRouter } = require('./routers/loadsRouter');

const corsOptions = {
  origin:'*', 
  credentials:true,
  optionSuccessStatus:200,
}
app.use(cors(corsOptions));
app.use(express.json());
app.use(morgan('tiny'));

app.use('/api/loads', loadsRouter);
app.use('/api/auth', authRouter);
app.use('/api/users', usersRouter);
app.use('/api/trucks', trucksRouter);

const start = async () => {
  try {
    app.listen(8080);
  } catch (err) {
    console.error(`Error on server startup: ${err.message}`);
  }
};

start();

// ERROR HANDLER
app.use(errorHandler);

function errorHandler(err, req, res) {
  return res.json({ message: err.message });
}

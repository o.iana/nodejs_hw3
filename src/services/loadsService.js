const { getTruckDimensions } = require('../utils/getTruckDimensions');

const truckTypes = ['SPRINTER', 'SMALL STRAIGHT', 'LARGE STRAIGHT'];

const check4Trucks = (load) => {
    return truckTypes.find((tr) => {
        let truckDimensions = getTruckDimensions(tr);
        return truckDimensions.payload > load.payload && truckDimensions.length > load.dimensions.length 
            && truckDimensions.width > load.dimensions.width && truckDimensions.height > load.dimensions.height  
    })
}

module.exports = {
    check4Trucks
}
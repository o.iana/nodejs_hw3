// const nodemailer = require('nodemailer')

// const transporter = nodemailer.createTransport({
//     auth: {
//         user: process.env.EMAIL,
//         pass: process.env.PASSWORD
//     }
// })

// const getPasswordResetURL = (user, token) => {
//     `http://localhost:8080/password/reset/${user._id}/${token}`
// }

// const resetPasswordTemplate = (user, url) => {
//     console.log('process.env.EMAIL', process.env.EMAIL);
//     console.log('***** user.email *****', user.email);
//     const from = process.env.EMAIL
//     const to = user.email
//     const subject = 'Password Reset'
//     const html = `
//         <p>Hey ${user.name || user.email},</p>
//         <p>We heard that you forgot your password. Sorry about that!</p>
//         <p>But don’t worry! You can use the following link to reset your password:</p>
//         <a href=${url}>${url}</a>
//         <p>If you don’t use this link within 1 hour, it will expire.</p>
//     `
//     return { from, to, subject, html }
// }

// module.exports = {transporter, getPasswordResetURL, resetPasswordTemplate}
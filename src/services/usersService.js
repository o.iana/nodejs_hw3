const { User } = require('../models/Users');
const bcryptjs = require('bcryptjs');

const saveUser = async ({ email, password, role, created_date }) => {
  try {
    const user = new User({
      email,
      password: await bcryptjs.hash(password, 10),
      role,
      created_date
    });
    return await user.save();
  } catch (error) {
    return error;
  }

}

module.exports = {
  saveUser
}

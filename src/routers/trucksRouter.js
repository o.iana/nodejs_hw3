const express = require('express');

const router = new express.Router();
const {
  addTruck, getTruck, deleteTruck, updateTruck, getAllTrucks,
  assignTruckToUser,
} = require('../controllers/trucksController');

const { authMiddleware } = require('../middleware/authMiddleware');

router.post('/', authMiddleware, addTruck);

router.get('/:id', authMiddleware, getTruck);

router.get('/', authMiddleware, getAllTrucks);

router.post('/:id/assign', authMiddleware, assignTruckToUser);

router.put('/:id', authMiddleware, updateTruck);

router.delete('/:id', authMiddleware, deleteTruck);

router.all('*', function(req, res) {
  res.redirect('http://localhost:8080/api/auth/register');
});

module.exports = {
  trucksRouter: router,
};

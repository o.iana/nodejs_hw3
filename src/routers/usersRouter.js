const express = require('express');
const router = new express.Router();
const { getUserInfo, updateUserPassword, deleteUser } = require('../controllers/usersController');
const { authMiddleware } = require('../middleware/authMiddleware');

router.get('/me',authMiddleware, getUserInfo);

router.delete('/me', authMiddleware, deleteUser);

router.patch('/me/password', authMiddleware, updateUserPassword);

router.all('*', function(req, res) {
  res.redirect('http://localhost:8080/api/auth/register');
});

module.exports = {
  usersRouter: router,
};

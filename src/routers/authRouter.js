const express = require('express');
const router = new express.Router();
const { registerUser, loginUser,restorePass } = require('../controllers/authController');
// const { sendPasswordResetEmail } = require('../controllers/mailController');

const asyncWrapper = (controller) => {
  return (req, res, next) => controller(req, res, next).catch(next);
}

router.post('/register', asyncWrapper(registerUser));

router.post('/login', asyncWrapper(loginUser));

router.post('/forgot_password', asyncWrapper(restorePass));

module.exports = {
  authRouter: router,
};

const express = require('express');
const router = new express.Router();
const {
  getUserLoads, addLoad, getUserLoadById, deleteUserLoadById, updateUserLoadById, 
  iterateLoadState, getActiveLoad, postUserLoadById, getLoadShipInfo
} = require('../controllers/loadsController');
const { authMiddleware } = require('../middleware/authMiddleware');

router.get('/', authMiddleware, getUserLoads);

router.post('/', authMiddleware, addLoad);

router.get('/active', authMiddleware, getActiveLoad);

router.patch('/active/state', authMiddleware, iterateLoadState);

router.get('/:id', authMiddleware, getUserLoadById);

router.put('/:id', authMiddleware, updateUserLoadById);

router.delete('/:id', authMiddleware, deleteUserLoadById);

router.post('/:id/post', authMiddleware, postUserLoadById);

router.get('/:id/shipping_info', authMiddleware, getLoadShipInfo);

module.exports = {
  loadsRouter: router,
};
